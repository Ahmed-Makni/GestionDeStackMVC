package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.LigneDeCommandeFournisseur;

public interface ILigneDeCommandeFournisseurService {
	public LigneDeCommandeFournisseur save(LigneDeCommandeFournisseur entity);

	public LigneDeCommandeFournisseur update(LigneDeCommandeFournisseur entity);

	public List<LigneDeCommandeFournisseur> selectAll();

	public List<LigneDeCommandeFournisseur> selectAll(String sortField, String sort);

	public LigneDeCommandeFournisseur getById(Long id);

	public void remove(Long id);

	public LigneDeCommandeFournisseur findOne(String paramName, Object paramValue);

	public LigneDeCommandeFournisseur findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);
}
