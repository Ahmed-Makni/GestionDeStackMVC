package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneDeVenteDao;
import com.stock.mvc.entites.LigneDeVente;
import com.stock.mvc.services.ILigneDeVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneDeVenteService {

	private ILigneDeVenteDao dao;

	public void setDao(ILigneDeVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneDeVente save(LigneDeVente entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneDeVente update(LigneDeVente entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneDeVente> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneDeVente> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneDeVente getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);

	}

	@Override
	public LigneDeVente findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneDeVente findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
