package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.LigneDeVente;

public interface ILigneDeVenteService {
	public LigneDeVente save(LigneDeVente entity);

	public LigneDeVente update(LigneDeVente entity);

	public List<LigneDeVente> selectAll();

	public List<LigneDeVente> selectAll(String sortField, String sort);

	public LigneDeVente getById(Long id);

	public void remove(Long id);

	public LigneDeVente findOne(String paramName, Object paramValue);

	public LigneDeVente findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);
}
