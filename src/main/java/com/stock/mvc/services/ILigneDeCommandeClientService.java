package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.LigneDeCommandeClient;

public interface ILigneDeCommandeClientService {
	public LigneDeCommandeClient save(LigneDeCommandeClient entity);

	public LigneDeCommandeClient update(LigneDeCommandeClient entity);

	public List<LigneDeCommandeClient> selectAll();

	public List<LigneDeCommandeClient> selectAll(String sortField, String sort);

	public LigneDeCommandeClient getById(Long id);

	public void remove(Long id);

	public LigneDeCommandeClient findOne(String paramName, Object paramValue);

	public LigneDeCommandeClient findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

	public List<LigneDeCommandeClient> getByIdCommande(Long idCommandeClient);
}
