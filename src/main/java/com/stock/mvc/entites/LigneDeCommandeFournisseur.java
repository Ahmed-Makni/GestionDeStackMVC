package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class LigneDeCommandeFournisseur implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="idLCF")
	private Long idLCF;
	
	
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	
	@ManyToOne
	@JoinColumn(name="idCF")
	private CommandeFournisseur commandeFournisseur;


	public Long getIdLCF() {
		return idLCF;
	}


	public void setIdLCF(Long idLCF) {
		this.idLCF = idLCF;
	}


	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}


	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}


	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}


	public LigneDeCommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
