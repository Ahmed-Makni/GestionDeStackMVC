package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Client implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="idC")
	private Long idC;
	private String nom;
	private String prenom;
	private String adresse;
	private String photo;
	private String mail;
	
	
	@OneToMany(mappedBy="client")
	private List<CommandeClient> CommandeClients;
	
	
	
	
	
	
	public Long getIdC() {
		return idC;
	}
	public void setIdC(Long idC) {
		this.idC = idC;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getMail() { 
		return mail;
	}
	public Client(Long idC, String nom, String prenom, String adresse, String photo, String mail) {
		super();
		this.idC = idC;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
		this.mail = mail;
	}

	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Client(Long idC, String nom, String prenom, String adresse, String photo) {
	
		this.idC = idC;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
	}
	
	public Client( String nom, String prenom, String adresse, String photo, String mail) {
	
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
		this.mail = mail;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
