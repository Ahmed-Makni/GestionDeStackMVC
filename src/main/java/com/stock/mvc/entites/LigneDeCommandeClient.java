package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class LigneDeCommandeClient implements Serializable{


	@Id
	@GeneratedValue
	@Column(name="idLigneDeCommande")
	private Long idF;
	
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	
	@ManyToOne
	@JoinColumn(name="idCommandeClient")
	private CommandeClient CommandeClient;
	
	
	
	public Long getIdF() {
		return idF;
	}



	public void setIdF(Long idF) {
		this.idF = idF;
	}



	public Article getArticle() {
		return article;
	}



	public void setArticle(Article article) {
		this.article = article;
	}



	public CommandeClient getCommandeClient() {
		return CommandeClient;
	}



	public void setCommandeClient(CommandeClient commandeClient) {
		CommandeClient = commandeClient;
	}



	public LigneDeCommandeClient() {
		// TODO Auto-generated constructor stub
	}
}
