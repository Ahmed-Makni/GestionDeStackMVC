package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class LigneDeVente implements Serializable{

	
	@Id
	@GeneratedValue
	@Column(name="idLV")
	private Long idLV;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="vente")
	private Vente vente;
	
	
	public LigneDeVente() {
		// TODO Auto-generated constructor stub
	}


	public Long getIdLV() {
		return idLV;
	}


	public void setIdLV(Long idLV) {
		this.idLV = idLV;
	}


	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	
	

}
