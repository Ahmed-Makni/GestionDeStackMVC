package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class CommandeFournisseur implements Serializable{

	
	@Id
	@GeneratedValue
	@Column(name="idCF")
	private Long idCF;
	
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	
	@ManyToOne
	@JoinColumn(name="idF")
	private Fournisseur fournisseur;
	
	
	
	@OneToMany(mappedBy="commandeFournisseur")
	private List<LigneDeCommandeFournisseur> ligneDeCommandeFournisseurs;



	public Long getIdCF() {
		return idCF;
	}



	public void setIdCF(Long idCF) {
		this.idCF = idCF;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public Date getDateCommande() {
		return dateCommande;
	}



	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}



	public Fournisseur getFournisseur() {
		return fournisseur;
	}



	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}



	public List<LigneDeCommandeFournisseur> getLigneDeCommandeFournisseurs() {
		return ligneDeCommandeFournisseurs;
	}



	public void setLigneDeCommandeFournisseurs(List<LigneDeCommandeFournisseur> ligneDeCommandeFournisseurs) {
		this.ligneDeCommandeFournisseurs = ligneDeCommandeFournisseurs;
	}



	public CommandeFournisseur() {
		super();
	}
	
	
	
	
	
}
