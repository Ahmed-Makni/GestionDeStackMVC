package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entites.LigneDeCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneDeCommandeClient> {

	public List<LigneDeCommandeClient> getByIdCommande(Long idCommandeClient);
}
