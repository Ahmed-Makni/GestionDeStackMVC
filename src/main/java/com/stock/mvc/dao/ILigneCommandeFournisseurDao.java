package com.stock.mvc.dao;

import com.stock.mvc.entites.LigneDeCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneDeCommandeFournisseur> {

}
