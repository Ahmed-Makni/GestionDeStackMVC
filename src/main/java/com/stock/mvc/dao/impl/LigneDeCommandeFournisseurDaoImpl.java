package com.stock.mvc.dao.impl;

import com.stock.mvc.dao.ILigneCommandeFournisseurDao;
import com.stock.mvc.entites.LigneDeCommandeFournisseur;

public class LigneDeCommandeFournisseurDaoImpl extends GenericDaoImpl<LigneDeCommandeFournisseur>
		implements ILigneCommandeFournisseurDao {

}
