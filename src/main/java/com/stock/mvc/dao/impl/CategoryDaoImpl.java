package com.stock.mvc.dao.impl;

import java.util.List;

import javax.persistence.Query;

import com.stock.mvc.dao.ICategorieDao;
import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Category;

public class CategoryDaoImpl extends GenericDaoImpl<Category> implements ICategorieDao {

	@Override
	public List<Article> selectAllArticlesByCategory(Long idCategory) {
		Query query = em
				.createQuery("select a from " + Article.class.getSimpleName() + " a where a.category.idCategory = :x");
		query.setParameter("x", idCategory);
		return query.getResultList();
	}

}
