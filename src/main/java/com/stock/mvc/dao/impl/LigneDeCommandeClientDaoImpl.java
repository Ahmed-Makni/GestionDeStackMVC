package com.stock.mvc.dao.impl;

import java.util.List;

import javax.persistence.Query;

import com.stock.mvc.dao.ILigneCommandeClientDao;
import com.stock.mvc.entites.LigneDeCommandeClient;

public class LigneDeCommandeClientDaoImpl extends GenericDaoImpl<LigneDeCommandeClient>
		implements ILigneCommandeClientDao {

	@Override
	public List<LigneDeCommandeClient> getByIdCommande(Long idCommandeClient) {
		String queryString = "select lc from " + LigneDeCommandeClient.class.getSimpleName()
				+ " lc where lc.commandeClient.idCommandeClient = :x";
		Query query = em.createQuery(queryString);
		query.setParameter("x", idCommandeClient);
		return query.getResultList();
	}

}
